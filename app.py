from flask import Flask, jsonify, make_response, request
from flask.ext.cors import CORS
from sqlalchemy import *
import json
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow



app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://hesham:hesham@localhost/test1'

CORS(app)
db = SQLAlchemy(app)
ma = Marshmallow(app)

tasks = []


class tableList(db.Model):
    __tablename__ = 'list'
    id = Column(Integer, primary_key=True)
    title = Column(String)
    descr = Column(String)
    done = Column(String)


class ListSchema(ma.ModelSchema):
    class Meta:
        model = tableList


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'not found!'}))


@app.route("/todo/api/v1.0/tasks", methods=['GET'])
def get_tasks():
    session = db.session()

    Schema = ListSchema(many=True)
    task = session.query(tableList).all()
    json_list = Schema.dump(task)
    return json.dumps(json_list.data)


@app.route("/todo/api/v1.0/post", methods=['POST'])
def create_task():
    session = db.session()

    if 'id' in request.json:
        id = request.json['id']

        title = request.json['title']
        descr = request.json['descr']
        done = False
        task = tableList(id=id, title=title, descr=descr, done=done)
    else:

        title = request.json['title']
        descr = request.json['descr']
        done = False
        task = tableList(title=title, descr=descr, done=done)


    session.add(task)
    session.commit()
    return jsonify({'msg': 'OK'}), 201


@app.route("/todo/api/v1.0/del", methods=['POST'])
def del_task():
    session = db.session()
    id = int(float(request.json['id']))

    task = session.query(tableList).get(id)

    session.delete(task)
    session.commit()
    return jsonify({'msg': 'OK'}), 200


@app.route("/todo/api/v1.0/done", methods=['POST'])
def done_task():
    session = db.session()
    id = int(float(request.json['id']))
    task = session.query(tableList).get(id)
    print (id)
    if task.done == 'True':
        task.done = "False"
    else:
        task.done = "True"

    session.commit()

    return jsonify({'msg': 'OK'}), 200

@app.route("/todo/api/v1.0/edit", methods=['POST'])
def edit_task():
    session = db.session()
    id = request.json['id']
    descr = request.json['descr']
    title = request.json['title']
    task = session.query(tableList).get(id)
    task.descr = descr
    task.title = title

    session.commit()

    return jsonify({'msg': 'OK'}), 200

if __name__ == '__main__':
    app.run(debug=True)
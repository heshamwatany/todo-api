import app
import unittest
import json

tasks = '{"done": "True", "id": 40, "descr": "\\u064a\\u0628\\u064a\\u0628", "title": "\\u0628\\u064a\\u0628\\u064a\\u0628"}'
submittask = {"id": "40", "title": "title new", "descr": "hello world"}
editTask = {"id": "40", "title": "title new", "descr": "hello world"}
delTask = {"id": "40"}
doneTask = {"id": "40"}


class Testinit(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.client = app.app.test_client()
        self.client.testing = True

    def tearDown(self):
        pass


    def test_GetTaskData(self):
        get = self.client.get('/todo/api/v1.0/tasks')
        assert tasks in get.data

    def test_SubmitTask(self):
        post = self.client.post('/todo/api/v1.0/post', data=json.dumps(submittask), content_type="application/json")
        self.assertEqual(post.status_code, 201)

    def test_EditTask(self):
        edit = self.client.post('/todo/api/v1.0/edit', data=json.dumps(editTask), content_type="application/json")
        self.assertEqual(edit.status_code, 200)

    def test_doneTask(self):
        done = self.client.post('/todo/api/v1.0/done', data=json.dumps(doneTask), content_type="application/json")
        self.assertEqual(done.status_code, 200)

    def test_deleteTask(self):
        delete = self.client.post('/todo/api/v1.0/del', data=json.dumps(delTask), content_type="application/json")
        self.assertEqual(delete.status_code, 200)


if __name__ == '__main__':
    unittest.main()
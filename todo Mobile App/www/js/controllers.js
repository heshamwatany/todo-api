angular.module('myApp')

  .controller("myCtrl", function ($scope, $http) {


    $scope.showedit = false;
    $scope.fields = {};
    $scope.editarray = {};

    gettasks();

    function gettasks() {
      $http.get("http://127.0.0.1:5000/todo/api/v1.0/tasks")
        .then(function (response) {
          $scope.lists = response.data;
        }
      );
    }

    $scope.remove = function (RemoveId) {
      $http.post("http://127.0.0.1:5000/todo/api/v1.0/del", {id: RemoveId})
        .then(gettasks);
    };

    $scope.done = function (ChkID) {
      $http.post("http://127.0.0.1:5000/todo/api/v1.0/done", {id: ChkID})
        .then(
        gettasks
      );

    };

    $scope.edit = function (ChkID) {
      $scope.editarray.id = ChkID;

      $http.post("http://127.0.0.1:5000/todo/api/v1.0/edit", $scope.editarray).then(
        gettasks
      );


    };

    $scope.submit = function () {
      $http.post("http://127.0.0.1:5000/todo/api/v1.0/post", $scope.fields).then(
        gettasks
      );

    };
  }
);

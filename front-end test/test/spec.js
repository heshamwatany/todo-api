'use strict';

describe('Testing myCtrl', function () {

    var $httpBackend, $rootScope, createController, tasksRequestHandler;
    var getURL = 'http://127.0.0.1:5000/todo/api/v1.0/tasks';
    var postURL = 'http://127.0.0.1:5000/todo/api/v1.0/post';
    var editURL = 'http://127.0.0.1:5000/todo/api/v1.0/edit';
    var delURL = 'http://127.0.0.1:5000/todo/api/v1.0/del';
    var doneURL = 'http://127.0.0.1:5000/todo/api/v1.0/done';
    var badURL = 'http://127.0.0.1:5000/todo/api/v1.0/';

    var successInput = [{'id': '1', 'title': 'New Task Title', 'descr': 'Describing task', 'done': 'True'}];
    var successOutput = null;
    var scope = null;


    beforeEach(function () {
        module('myApp');
    });


    beforeEach(inject(function ($injector) {
        $httpBackend = $injector.get('$httpBackend');

        tasksRequestHandler = $httpBackend.when('GET', getURL)
            .respond(successInput);

        $rootScope = $injector.get('$rootScope');
        var $controller = $injector.get('$controller');

        scope = $rootScope.$new();

        createController = function () {
            return $controller('myCtrl', {'$scope': scope});
        };
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    //when loading/reloading the document
    it('should get tasks from the server', function () {
        $httpBackend.expectGET(getURL)
            .respond(200, successInput);
        var controller = createController();
        $httpBackend.flush();
        expect(scope.lists).toEqual(successInput);
    });

    //when submitting new task
    it('should send tasks to the server', function () {
        $httpBackend.expectGET(getURL)
            .respond(200, successInput);
        var controller = createController();
        $httpBackend.flush();
        expect(scope.lists).toEqual(successInput);

        scope.fields = {title: 'task 1'};

        $httpBackend.expectPOST(postURL, {title: 'task 1'})
            .respond(201, '');

        scope.submit();

        $httpBackend.flush();
    });

    //when deleting  task
    it('should delete task from the server', function () {

        var controller = createController();

        var ID = '1'

        $httpBackend.expectPOST(delURL, {'id': '1'})
            .respond(201, '');

        scope.remove(ID);

        $httpBackend.flush();
    });

    //when done a  task
    it('should change task to DONE in the server database', function () {

        var controller = createController();

        var ID = '1'

        $httpBackend.expectPOST(doneURL, {'id': '1'})
            .respond(201, '');

        scope.done(ID);

        $httpBackend.flush();
    });

    //when editing a task
    it('should change task data in the server database', function () {

        var controller = createController();
        var ChkID = "1"
        scope.editarray = [{"id": "1", "title": "New Task Title", "descr": "Describing task", "done": "True"}];
        scope.editarray.id = ChkID;

        $httpBackend.expectPOST(editURL, successInput)
            .respond(201, '');

        scope.edit(ChkID);

        $httpBackend.flush();
    });

});